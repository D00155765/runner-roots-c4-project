package com.project.android.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.project.android.R;
import java.util.ArrayList;

import com.project.android.activities.ChatActivity;
import com.project.android.activities.CustomApplication;
import com.project.android.classes.UserAccount;

import de.hdodenhof.circleimageview.CircleImageView;

public class FriendAdapter extends ArrayAdapter<UserAccount> {
    private ArrayList<UserAccount> userList;
    private LayoutInflater vi;
    private int Resource;
    private ViewHolder holder;
    private Context mContext;

    private CustomApplication application;

    public FriendAdapter(Context context, int resource, ArrayList<UserAccount> objects) {
        super(context, resource, objects);
        vi = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Resource = resource;
        userList = objects;
        this.mContext = context;
        application = (CustomApplication) context.getApplicationContext();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // convert view = design

        Typeface mainFont = Typeface.createFromAsset(getContext().getAssets(), "fonts/Titillium-RegularUpright.otf");
        Typeface shadeFont = Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto-Light.ttf");
        View v = convertView;
        if (v == null) {
            holder = new ViewHolder();
            v = vi.inflate(Resource, null);
            holder.profile_pic = (CircleImageView) v.findViewById(R.id.profile_pic);
            holder.user_name = (TextView) v.findViewById(R.id.user_name);
            holder.user_country = (TextView) v.findViewById(R.id.user_country);
            holder.send_message = (FloatingActionButton) v.findViewById(R.id.send_message);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        final int i = position;
        Glide.with(getContext()).load(userList.get(position).getProfile().getImageURL()).into(holder.profile_pic);
        holder.user_name.setText(userList.get(position).getProfile().getFname() + ", " + userList.get(position).getProfile().getAge());
        holder.user_country.setText(userList.get(position).getProfile().getCountry());

        holder.user_name.setTypeface(mainFont);
        holder.user_country.setTypeface(shadeFont);

        holder.send_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ChatActivity.class);
                intent.putExtra("profile", userList.get(position).getProfile());
                mContext.startActivity(intent);
            }
        });

        return v;
    }

    static class ViewHolder {
        public CircleImageView profile_pic;
        public TextView user_name;
        public FloatingActionButton send_message;
        public TextView user_country;
    }
}