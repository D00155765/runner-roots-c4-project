package com.project.android.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.project.android.R;
import com.project.android.activities.ChatActivity;
import com.project.android.activities.CustomApplication;
import com.project.android.classes.ChatItem;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Cormac on 30/04/2017.
 */

public class ChatItemAdapter extends ArrayAdapter<ChatItem> {
    private ArrayList<ChatItem> chatItemList;
    private LayoutInflater vi;
    private int Resource;
    private ViewHolder holder;

    public ChatItemAdapter(Context context, int resource, ArrayList<ChatItem> objects) {
        super(context, resource, objects);
        this.vi = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.Resource = resource;
        this.chatItemList = objects;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Typeface timestampFont = Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto-Regular.ttf");
        Typeface shadeFont = Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto-Light.ttf");
        Typeface mainFont = Typeface.createFromAsset(getContext().getAssets(), "fonts/Titillium-RegularUpright.otf");

        View v = convertView;

        if (v == null) {
            holder = new ViewHolder();
            v = vi.inflate(Resource, null);
            holder.profile_pic = (CircleImageView) v.findViewById(R.id.profile_pic);
            holder.user_name = (TextView) v.findViewById(R.id.user_name);
            holder.last_message = (TextView) v.findViewById(R.id.last_message);
            holder.date = (TextView) v.findViewById(R.id.date);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        final int i = position;
        Glide.with(getContext()).load(chatItemList.get(position).getImageURL()).into(holder.profile_pic);
        holder.user_name.setText(chatItemList.get(position).getName() + ", " + chatItemList.get(position).getCountry());
        holder.last_message.setText(chatItemList.get(position).getLast_message());
        holder.date.setText(chatItemList.get(position).getDate());

        holder.user_name.setTypeface(mainFont);
        holder.last_message.setTypeface(shadeFont);
        holder.date.setTypeface(timestampFont);

        return v;
    }

    static class ViewHolder {
        public CircleImageView profile_pic;
        public TextView user_name;
        public TextView last_message;
        public TextView date;

    }
}