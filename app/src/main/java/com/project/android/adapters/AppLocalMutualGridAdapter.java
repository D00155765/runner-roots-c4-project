package com.project.android.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.project.android.R;
import com.project.android.activities.CustomApplication;
import com.project.android.classes.UserAccount;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Cormac on 29/04/2017.
 */

public class AppLocalMutualGridAdapter extends ArrayAdapter<UserAccount> {

    ArrayList<UserAccount> app_local_mutuals = new ArrayList<>();

    public AppLocalMutualGridAdapter(Context context, int resourceId, ArrayList<UserAccount> list) {
        super(context, resourceId, list);
        app_local_mutuals = list;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        v = inflater.inflate(R.layout.grid_item, null);



        TextView textView = (TextView) v.findViewById(R.id.fname);
        CircleImageView imageView = (CircleImageView) v.findViewById(R.id.profile_pic);


        textView.setText(app_local_mutuals.get(position).getProfile().getName());
        Glide.with(getContext()).load(app_local_mutuals.get(position).getProfile().getImageURL()).dontAnimate().into(imageView);

        return v;
    }
}