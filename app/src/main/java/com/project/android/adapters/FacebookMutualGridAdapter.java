package com.project.android.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.Resource;
import com.project.android.R;
import com.project.android.activities.CustomApplication;
import com.project.android.classes.Friends;

import java.util.ArrayList;
import de.hdodenhof.circleimageview.CircleImageView;

import static java.security.AccessController.getContext;

/**
 * Created by Cormac on 22/04/2017.
 */

public class FacebookMutualGridAdapter extends ArrayAdapter<Friends> {

    ArrayList<Friends> friends = new ArrayList<>();

    public FacebookMutualGridAdapter(Context context, int resourceId, ArrayList<Friends> list) {
        super(context, resourceId, list);
        friends = list;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.grid_item, null);

        TextView textView = (TextView) v.findViewById(R.id.fname);
        CircleImageView imageView = (CircleImageView) v.findViewById(R.id.profile_pic);

        textView.setText(friends.get(position).getName());
        Glide.with(getContext()).load(friends.get(position).getImageURL()).dontAnimate().into(imageView);

        return v;
    }
}