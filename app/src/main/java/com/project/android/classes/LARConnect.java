package com.project.android.classes;

import android.app.Activity;
import android.util.Log;

import com.project.android.activities.CustomApplication;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Cormac on 22/03/2017.
 */

public class LARConnect {
    private static final int COUNTRY = 20;
    private static final int AGE = 10;
    private static final int GENDER = 10;
    private static final int RELATIONAL = 40;
    private static final int AFFILIATION = 20;

    public LARConnect() {
    }


    public static Map<String, UserAppData> LARCSort(HashMap<String, UserAppData> map, UserAccount user, Activity activity) {

        int total_score;
        int aff_score;

        for (String key : map.keySet()) {

            if (map.containsKey(key)) {
                total_score = 0;
                aff_score = 0;

                // Checks if country is the same
                if (map.get(key).getUser().getProfile().getCountry().equals(user.getProfile().getCountry())) {
                    // increase LARC score
                    total_score = total_score + COUNTRY;
                }

                // Checks if gender is the same
                if (map.get(key).getUser().getProfile().getGender().equals(user.getProfile().getGender())) {
                    // increase LARC score
                    total_score += GENDER;
                }

                // Finds the age difference between two users
                int age_difference = map.get(key).getUser().getProfile().getAge() - user.getProfile().getAge();

                // Based on age difference, increases LARC score
                if (age_difference <= 5 && age_difference >= -5) {
                    total_score += AGE;
                } else if (age_difference <= 10 && age_difference >= -10) {
                    total_score += AGE * 0.5;
                } else {
                    total_score += 0;
                }

                if (map.get(key).getUser().getProfile().getInterests() != null && user.getProfile().getInterests() != null) {
                    for (int j = 0; j < map.get(key).getUser().getProfile().getInterests().size(); j++) {
                        for (int z = 0; z < user.getProfile().getInterests().size(); z++) {
                            if (user.getProfile().getInterests().get(z).equals(map.get(key).getUser().getProfile().getInterests().get(j))) {
                                aff_score++;
                            }
                        }
                    }
                }

                // increase LARC score based on affiliation and relation count taken from
                // the FilterResults class and LoadDataActivity

                if (aff_score >= 3) {
                    total_score += AFFILIATION;
                } else if (aff_score >= 2) {
                    total_score += AFFILIATION * 0.75;
                } else if (aff_score >= 1) {
                    total_score += AFFILIATION * 0.50;
                } else {
                    total_score += 0;
                }

                if (map.get(key).getRelationNum() >= 5) {
                    total_score += RELATIONAL;
                } else if (map.get(key).getRelationNum() >= 3) {
                    total_score += RELATIONAL * 0.75;
                } else if (map.get(key).getRelationNum() >= 2) {
                    total_score += RELATIONAL * 0.5;
                } else if (map.get(key).getRelationNum() == 1) {
                    total_score += RELATIONAL * 0.25;
                } else {
                    total_score += 0;
                }

                // Sets user score in map obect
                map.get(key).setScore(total_score);
            } else {
                break;
            }
        }

        // Sorts HashMap based on Score Value
        Map<String, UserAppData> sortedMap = sortByValue(map);

        return sortedMap;
    }


    // HashMap Sort By Value - Score Value

    private static Map<String, UserAppData> sortByValue(Map<String, UserAppData> unsortMap) {

        List<Map.Entry<String, UserAppData>> list =
                new LinkedList<Map.Entry<String, UserAppData>>(unsortMap.entrySet());

        Collections.sort(list, new Comparator<Map.Entry<String, UserAppData>>() {
            public int compare(Map.Entry<String, UserAppData> o1,
                               Map.Entry<String, UserAppData> o2) {
                return (Integer.compare(o2.getValue().getScore(), o1.getValue().getScore()));
            }
        });

        Map<String, UserAppData> sortedMap = new LinkedHashMap<String, UserAppData>();
        for (Map.Entry<String, UserAppData> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }
}


