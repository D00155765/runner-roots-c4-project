package com.project.android.classes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Cormac on 21/04/2017.
 */
// Object Class for sending and retrieving data to Firebase accounts Document
public class UserAccount implements Serializable {

    private UserProfile profile;
    private HashMap<String, UserAccount> friends;
    private HashMap<String, UserAccount> connections;
    private HashMap<String, UserAccount> friendRequests;
    private HashMap<String, UserAccount> connectionRequests;

    public UserAccount(UserProfile profile, HashMap<String, UserAccount> friends, HashMap<String, UserAccount> connections,
                       HashMap<String, UserAccount> friendRequests, HashMap<String, UserAccount> connectionRequests) {


        this.profile = profile;
        this.friends = friends;
        this.connections = connections;
        this.friendRequests = friendRequests;
        this.connectionRequests = connectionRequests;
    }

    public UserAccount() {
    }

    public UserProfile getProfile() {
        return profile;
    }

    public void setProfile(UserProfile profile) {
        this.profile = profile;
    }

    public HashMap<String, UserAccount> getConnectionRequests() {
        return connectionRequests;
    }

    public void setConnectionRequests(HashMap<String, UserAccount> connectionRequests) {
        this.connectionRequests = connectionRequests;
    }

    public HashMap<String, UserAccount> getFriendRequests() {
        return friendRequests;
    }

    public void setFriendRequests(HashMap<String, UserAccount> friendRequests) {
        this.friendRequests = friendRequests;
    }

    public HashMap<String, UserAccount> getConnections() {
        return connections;
    }

    public void setConnections(HashMap<String, UserAccount> connections) {
        this.connections = connections;
    }

    public HashMap<String, UserAccount> getFriends() {
        return friends;
    }

    public void setFriends(HashMap<String, UserAccount> friends) {
        this.friends = friends;
    }

    @Override
    public String toString() {
        return "UserAccount{" +
                "profile=" + profile +
                ", friends=" + friends +
                ", connections=" + connections +
                ", friendRequests=" + friendRequests +
                ", connectionRequests=" + connectionRequests +
                '}';
    }
}