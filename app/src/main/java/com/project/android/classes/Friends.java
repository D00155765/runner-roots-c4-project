package com.project.android.classes;

import java.io.Serializable;

/**
 * Created by Cormac on 28/03/2017.
 */

// Object Class for Mutual Facebook Friends two users share

public class Friends implements Serializable
{
    private String sid;
    private String name;
    private String imageURL;

    public Friends(String sid, String name, String imageURL) {
        this.sid = sid;
        this.name = name;
        this.imageURL = imageURL;
    }

    public Friends()
    {}

    public String getId() {
        return sid;
    }

    public void setId(String id) {
        this.sid = sid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    @Override
    public String toString() {
        return "Friends{" +
                "sid='" + sid + '\'' +
                ", name='" + name + '\'' +
                ", imageURL='" + imageURL + '\'' +
                '}';
    }
}
