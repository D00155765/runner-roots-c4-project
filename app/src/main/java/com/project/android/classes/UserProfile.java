package com.project.android.classes;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Cormac on 27/02/2017.
 */

// Object Class for sending and retrieving data to Firebase profile child node of accounts Document
public class UserProfile implements Serializable {

    private String uid;
    private String sid;
    private String name;
    private String fname;
    private String country;
    private String dob;
    private String gender;
    private int age;
    private UserLocation location;
    private String imageURL;
    private String blurb;
    private ArrayList<String> interests;
    private int proximityDist;


    public UserProfile(String uid, String sid, String name, String fname, String country, String dob, String gender,
                       int age, UserLocation location, String imageURL, String blurb, ArrayList<String> interests, int proximityDist)
    {
        this.uid = uid;
        this.sid = sid;
        this.name = name;
        this.fname = fname;
        this.country = country;
        this.dob = dob;
        this.gender = gender;
        this.age = age;
        this.location = location;
        this.imageURL = imageURL;
        this.blurb = blurb;
        this.interests = interests;
        this.proximityDist = proximityDist;
    }

    public UserProfile()
    {

    }


    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public UserLocation getLocation() {
        return location;
    }

    public void setLocation(UserLocation location) {
        this.location = location;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getBlurb() {
        return blurb;
    }

    public void setBlurb(String blurb) {
        this.blurb = blurb;
    }

    public ArrayList<String> getInterests() {
        return interests;
    }

    public void setInterests(ArrayList<String> interests) {
        this.interests = interests;
    }

    public int getProximityDist() {
        return proximityDist;
    }

    public void setProximityDist(int proximityDist) {
        this.proximityDist = proximityDist;
    }


    public String getInterestString(ArrayList<String> list)
    {
        String str = "";

        for(int i = 0; i < list.size(); i++)
        {
            if(i == list.size() - 1) {
                str += list.get(i);
            }
            else
            {
                str += list.get(i) + ", ";
            }
        }

        return str;
    }

    @Override
    public String toString() {
        return "UserProfile{" +
                "uid='" + uid + '\'' +
                ", sid='" + sid + '\'' +
                ", name='" + name + '\'' +
                ", fname='" + fname + '\'' +
                ", country='" + country + '\'' +
                ", dob='" + dob + '\'' +
                ", gender='" + gender + '\'' +
                ", age=" + age +
                ", location=" + location +
                ", imageURL='" + imageURL + '\'' +
                ", blurb='" + blurb + '\'' +
                ", interests=" + interests +
                ", proximityDist=" + proximityDist +
                '}';
    }
}