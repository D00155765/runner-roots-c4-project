package com.project.android.classes;

import java.io.Serializable;

/**
 * Created by Cormac on 30/03/2017.
 */

// Object Class for storing Users location - latitude/ longitude.

public class UserLocation implements Serializable {

    UserLocation() {

    }

    private double latitude;
    private double longitude;

    public UserLocation(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "UserLocation{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
