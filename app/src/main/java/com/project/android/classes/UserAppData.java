package com.project.android.classes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Cormac on 21/04/2017.
 */
// Object Class for retrieving data from Firebase accounts Document and for whole app data processing and storing.

public class UserAppData implements Serializable {

    private UserAccount user;
    private ArrayList<UserAccount> local_mutual;
    private ArrayList<Friends> facebook_mutual;
    private int relationNum;
    private int score;
    private int proximity;
    private boolean inRange;
    private boolean show;


    public UserAppData(UserAccount user, ArrayList<UserAccount> local_mutual, ArrayList<Friends> facebook_mutual,
                       int relationNum, int score, int proximity, boolean inRange, boolean show) {
        this.user = user;
        this.local_mutual = local_mutual;
        this.facebook_mutual = facebook_mutual;
        this.relationNum = relationNum;
        this.score = score;
        this.proximity = proximity;
        this.inRange = inRange;
        this.show = show;
    }

    public UserAccount getUser() {
        return user;
    }

    public void setUser(UserAccount user) {
        this.user = user;
    }

    public ArrayList<Friends> getFacebook_mutual() {
        return facebook_mutual;
    }

    public void setFacebook_mutual(ArrayList<Friends> facebook_mutual) {
        this.facebook_mutual = facebook_mutual;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getProximity() {
        return proximity;
    }

    public void setProximity(int proximity) {
        this.proximity = proximity;
    }

    public boolean isShow() {
        return show;
    }

    public void setShow(boolean show) {
        this.show = show;
    }

    public int getRelationNum() {
        return relationNum;
    }

    public void setRelationNum(int relationNum) {
        this.relationNum = relationNum;
    }

    public ArrayList<UserAccount> getLocal_mutual() {
        return local_mutual;
    }

    public void setLocal_mutual(ArrayList<UserAccount> local_mutual) {
        this.local_mutual = local_mutual;
    }

    public boolean isInRange() {
        return inRange;
    }

    public void setInRange(boolean inRange) {
        this.inRange = inRange;
    }

    public void addItemToMutualList(Friends friend) {
        this.getFacebook_mutual().add(friend);
    }

    public void addItemToLocalMutualList(UserAccount user_account) {
        this.getLocal_mutual().add(user_account);
    }

    public int getLocalMutualCount() {
        return this.local_mutual.size();
    }

    public int getFacebookMutualCount() {
        return this.facebook_mutual.size();
    }

    @Override
    public String toString() {
        return "UserAppData{" +
                "user=" + user +
                ", facebook_mutual=" + facebook_mutual +
                ", score=" + score +
                ", proximity=" + proximity +
                ", show=" + show +
                ", relationNum=" + relationNum +
                ", local_mutual=" + local_mutual +
                ", inRange=" + inRange +
                '}';
    }
}
