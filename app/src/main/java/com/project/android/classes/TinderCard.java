package com.project.android.classes;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.mindorks.placeholderview.SwipePlaceHolderView;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;
import com.mindorks.placeholderview.annotations.swipe.SwipeCancelState;
import com.mindorks.placeholderview.annotations.swipe.SwipeIn;
import com.mindorks.placeholderview.annotations.swipe.SwipeInState;
import com.mindorks.placeholderview.annotations.swipe.SwipeOut;
import com.mindorks.placeholderview.annotations.swipe.SwipeOutState;
import com.project.android.R;
import com.project.android.activities.CustomApplication;
import com.project.android.activities.ProfileViewActivity;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import de.hdodenhof.circleimageview.CircleImageView;

import static java.security.AccessController.getContext;

// Source "https://github.com/janishar/PlaceHolderView" Accessed "12/03/17"

// This Class is for Instantiating data for the cardview view of LARConnectFragment.
@Layout(R.layout.tinder_card_view)
public class TinderCard {

    @View(R.id.profileImageView)
    private ImageView profileImageView;

    @View(R.id.score)
    private CircularProgressBar score;

    @View(R.id.nameAgeTxt)
    private TextView nameAgeTxt;

    @View(R.id.proximity_num)
    private TextView proximity_num;

    @View(R.id.connect_num)
    private TextView connect_num;

    @View(R.id.score_display)
    private TextView score_display;

    @View(R.id.locationNameTxt)
    private TextView locationNameTxt;

    private Typeface font;

    private UserAppData mProfile;
    private Context mContext;
    private SwipePlaceHolderView mSwipeView;

    private FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    private DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
    private CustomApplication application;

    public TinderCard(Context context, UserAppData profile, SwipePlaceHolderView swipeView) {
        mContext = context;
        mProfile = profile;
        mSwipeView = swipeView;
        font = Typeface.createFromAsset(mContext.getAssets(), "fonts/Titillium-RegularUpright.otf");
        application = (CustomApplication) context.getApplicationContext();
    }

    @Resolve
    private void onResolved() throws IOException {

        // Loading Content into Views that are in tinder card xml layout file.
        Glide.with(mContext).load(mProfile.getUser().getProfile().getImageURL()).into(profileImageView);

        if (mProfile.getScore() >= 70) {
            score.setColor(Color.parseColor("#F76014"));

        } else if (mProfile.getScore() >= 40 && mProfile.getScore() < 70) {
            score.setColor(Color.parseColor("#14B0BF"));
        } else {
            score.setColor(Color.parseColor("#66931F"));
        }

        score.setBackgroundColor(Color.parseColor("#FFFFFF"));
        score.setProgressBarWidth(20);
        score.setBackgroundProgressBarWidth(10);
        int animationDuration = 3000;
        score.setProgressWithAnimation(mProfile.getScore(), animationDuration);


        nameAgeTxt.setText(mProfile.getUser().getProfile().getFname() + ", " + mProfile.getUser().getProfile().getAge());
        nameAgeTxt.setTypeface(font);

        proximity_num.setText(Integer.toString(mProfile.getProximity()) + "km");
        proximity_num.setTypeface(font);

        connect_num.setText(Integer.toString(mProfile.getRelationNum()));
        connect_num.setTypeface(font);

        locationNameTxt.setText(mProfile.getUser().getProfile().getCountry());
        locationNameTxt.setTypeface(font);

        score_display.setText(Integer.toString(mProfile.getScore()) + "%");
        score_display.setTypeface(font);
    }


    // OnClick, open profile activity to see more information
    @Click(R.id.cardview)
    private void onClick() {

        application.setActiveProfile(mProfile);
        Intent intent = new Intent(mContext, ProfileViewActivity.class);
        mContext.startActivity(intent);
    }


    //On Swipe Up, send data to Firebase in the form of a connection request
    @SwipeOut
    private void onSwipedOut() {
        UserAccount temp = application.getMyData();
        temp.setFriendRequests(null);
        temp.setConnectionRequests(null);

        mDatabase.child("accounts").child(mProfile.getUser().getProfile().getUid()).child("connectionRequests").child(user.getUid()).setValue(temp);

    }

    @SwipeIn
    private void onSwipeIn() {
        Log.d("EVENT", "onSwipedOut");
        mSwipeView.addView(this);
    }

    @SwipeCancelState
    private void onSwipeCancelState() {
        Log.d("EVENT", "onSwipeCancelState");
    }

    @SwipeInState
    private void onSwipeInState() {
        Log.d("EVENT", "onSwipeInState");
    }

    @SwipeOutState
    private void onSwipeOutState() {
        Log.d("EVENT", "onSwipeOutState");
    }
}