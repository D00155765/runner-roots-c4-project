package com.project.android.classes;

import android.util.Log;

import java.util.HashMap;

/**
 * Created by Cormac on 28/04/2017.
 */

public class FilterResults {


    public FilterResults() {

    }

    // filterResults method removes all existing connections from app HashMap to only show profiles that you haven't connected to.
    public static HashMap<String, UserAppData> filterResults(HashMap<String, UserAppData> map, UserAccount user) {

        HashMap<String, UserAppData> filterMap = new HashMap<>();

        // Loading map into new map - need to iterate the map, and manipulate data in the filterMap
        for(String key : map.keySet())
        {
            filterMap.put(key, map.get(key));
        }


        if (map.keySet().size() > 0) {

            for (String key : map.keySet()) {
                // Checks if logged on user has got a request already from this user.
                if (user.getConnectionRequests() != null) {
                    for (String str : user.getConnectionRequests().keySet()) {
                        if (str.equals(key)) {
                            filterMap.remove(str);
                        }
                    }
                }
                // Checks if logged on user is already connected to this user.,
                if (user.getConnections() != null) {
                    for (String str : user.getConnections().keySet()) {
                        if (str.equals(key)) {
                            filterMap.remove(str);
                        }
                    }
                }
                // Checks if logged on user is already friends to this user.,
                if (user.getFriends() != null) {
                    for (String str : user.getFriends().keySet()) {
                        if (str.equals(key)) {
                            filterMap.remove(str);
                        }
                    }
                }

                // This method gets a count of how many mutual connections two users have with each other.
                if (filterMap.containsKey(key)) {

                    if (filterMap.get(key).getUser().getFriends() != null && user.getFriends() != null) {

                        for (String str : user.getFriends().keySet()) {
                            for (String otherStr : map.get(key).getUser().getFriends().keySet()) {
                                if (str.equals(otherStr)) {
                                    filterMap.get(key).addItemToLocalMutualList(user.getFriends().get(str));
                                }
                            }

                            if (filterMap.get(key).getUser().getConnections() != null){
                                for (String otherStr : map.get(key).getUser().getConnections().keySet()) {
                                    if (str.equals(otherStr)) {
                                        filterMap.get(key).addItemToLocalMutualList(user.getFriends().get(str));
                                    }
                                }
                            }
                        }
                    }
                }
                // This method gets a count of how many mutual friends two users have with each other.
                if (filterMap.containsKey(key)) {
                    if (filterMap.get(key).getUser().getConnections() != null && user.getConnections() != null) {

                        for (String str : user.getConnections().keySet()) {
                            if (map.get(key).getUser().getFriends() != null) {
                                for (String otherStr : map.get(key).getUser().getFriends().keySet()) {
                                    if (str.equals(otherStr)) {
                                        filterMap.get(key).addItemToLocalMutualList(user.getConnections().get(str));
                                    }
                                }
                            }

                            if (map.get(key).getUser().getConnections() != null) {
                                for (String otherStr : map.get(key).getUser().getConnections().keySet()) {
                                    if (str.equals(otherStr)) {
                                        filterMap.get(key).addItemToLocalMutualList(user.getConnections().get(str));
                                    }
                                }
                            }
                        }
                    }
                }

                // Set the relational num count to value taken from above code.
                if (filterMap.get(key) != null) {
                    filterMap.get(key).setRelationNum(filterMap.get(key).getFacebookMutualCount() + filterMap.get(key).getLocalMutualCount());
                }
            }

            // return map.
            return filterMap;
        }
        else
        {
            return map;
        }
    }

}

