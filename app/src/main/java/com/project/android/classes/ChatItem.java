package com.project.android.classes;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by Cormac on 30/04/2017.
 */

// Object Class for ListView Items in Messenger Fragment
public class ChatItem implements Serializable {

    private String uid;
    private String imageURL;
    private String name;
    private String country;
    private String last_message;
    private String date;

    public ChatItem(String uid, String imageURL, String name, String country, String last_message, String date) {
        this.uid = uid;
        this.imageURL = imageURL;
        this.name = name;
        this.country = country;
        this.last_message = last_message;
        this.date = date;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLast_message() {
        return last_message;
    }

    public void setLast_message(String last_message) {
        this.last_message = last_message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "ChatItem{" +
                "uid='" + uid + '\'' +
                ", imageURL='" + imageURL + '\'' +
                ", name='" + name + '\'' +
                ", country='" + country + '\'' +
                ", last_message='" + last_message + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}


