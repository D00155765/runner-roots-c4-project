package com.project.android.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.firebase.client.Firebase;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.project.android.R;
import com.project.android.classes.Friends;
import com.project.android.classes.User;
import com.project.android.classes.UserAccount;
import com.project.android.classes.UserProfile;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by Cormac on 27/02/2017.
 */

public class LoginActivity extends AppCompatActivity {

    public String TAG = "Runner Roots";

    // Firebase Auth Declarations
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private ProgressDialog mProgressDialog;

    // Global Variables
    private User user;
    private int age;
    private String uid, sid, fname, gender,
            imageURL, name, userEmail;

    // Firebase DB Declarations
    Firebase userRef = new Firebase("https://c4-project.firebaseio.com/users/");
    DatabaseReference accountRef = FirebaseDatabase.getInstance().getReference().child("accounts");

    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(com.project.android.R.layout.activity_login);

        // Logo View
        Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Caviar_Dreams_Bold.ttf");

        TextView firstLine = (TextView) findViewById(R.id.runner);
        firstLine.setTypeface(font);

        TextView secondLine = (TextView) findViewById(R.id.roots);
        secondLine.setTypeface(font);

        mAuth = FirebaseAuth.getInstance();

        // Checks if user is Logged In
        FirebaseUser mUser = mAuth.getCurrentUser();
        if (mUser != null) {

            //if true, start LoadDataActivity
            Intent intent = new Intent(getApplicationContext(), LoadDataActivity.class);
            startActivity(intent);
            finish();

            Log.d(TAG, "onAuthStateChanged:signed_in:" + mUser.getUid());
        }

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser mUser = firebaseAuth.getCurrentUser();
                if (mUser != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + mUser.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }

            }
        };

        //FaceBook
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        // Facebook Login Button and Permissions
        final LoginButton loginButton = (LoginButton) findViewById(R.id.button_facebook_login);

        loginButton.setReadPermissions("email", "public_profile", "user_friends");
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                showProgressDialog();
                Log.d(TAG, "facebook:onSuccess:" + loginResult);


                signInWithFacebook(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
            }
        });

        // Fancy Button to overlay Facebook Default Button
        FancyButton facebook_login = (FancyButton) findViewById(R.id.facebook_login);
        facebook_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginButton.performClick();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }


    //FaceBook
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    // Facebook Sign in method, called on Facebook Btn click
    private void signInWithFacebook(final AccessToken token) {
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        // If login unsuccessful
                        if (!task.isSuccessful()) {
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        } else {

                            // If login successful, initialise data, get Facebook Graph API data and set up UserAccont

                            uid = task.getResult().getUser().getUid();
                            name = task.getResult().getUser().getDisplayName();
                            userEmail = task.getResult().getUser().getEmail();
                            sid = Profile.getCurrentProfile().getId();
                            fname = Profile.getCurrentProfile().getFirstName();

                            user = new User(uid, name, userEmail, null, null);
                            userRef.child(uid).setValue(user);

                            GraphRequest request = GraphRequest.newMeRequest(token, new GraphRequest.GraphJSONObjectCallback() {
                                @Override
                                public void onCompleted(JSONObject object, GraphResponse response) {

                                    try {
                                        age = object.getJSONObject("age_range").getInt("min");
                                        gender = object.getString("gender");
                                        imageURL = object.getJSONObject("picture").getJSONObject("data").getString("url");

                                        if (imageURL.contains("_t.")) {
                                            imageURL = imageURL.replaceAll("_t.", "_n.");
                                        } else if (imageURL.contains("_a.")) {
                                            imageURL = imageURL.replaceAll("_a.", "_n.");
                                        } else if (imageURL.contains("_s.")) {
                                            imageURL = imageURL.replaceAll("_s.", "_n.");
                                        } else if (imageURL.contains("_q.")) {
                                            imageURL = imageURL.replaceAll("_q.", "_n.");
                                        }

                                        accountRef.child(uid).addListenerForSingleValueEvent
                                                (new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                                        if (dataSnapshot.getValue() != null) {

                                                            // If account already created, start LoadDataActivity
                                                            Intent intent = new Intent(getApplicationContext(), LoadDataActivity.class);
                                                            hideProgressDialog();
                                                            startActivity(intent);

                                                            finish();

                                                        } else {

                                                            // If account not created, create account, and start SignUpActivity
                                                            UserAccount user_account = new UserAccount(new UserProfile(uid, sid, name, fname, null, null, gender, age, null, imageURL, null, null, 50),
                                                                    null, null, null, null);

                                                            Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);
                                                            intent.putExtra("account", user_account);
                                                            hideProgressDialog();
                                                            startActivity(intent);

                                                            finish();
                                                        }
                                                    }

                                                    @Override
                                                    public void onCancelled(DatabaseError databaseError) {
                                                    }
                                                });
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                            Bundle parameters = new Bundle();
                            parameters.putString("fields", "gender, age_range, picture.height(961)");
                            request.setParameters(parameters);
                            request.executeAsync();
                        }
                    }
                });

    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Authenticating");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}

