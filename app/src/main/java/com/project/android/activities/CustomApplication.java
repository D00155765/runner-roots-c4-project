package com.project.android.activities;


import android.app.Application;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.firebase.client.Firebase;
import com.google.firebase.database.FirebaseDatabase;
import com.project.android.classes.UserAccount;
import com.project.android.classes.UserAppData;
import java.util.HashMap;

/**
 * Created by Cormac on 27/02/2017.
 */

public class CustomApplication extends Application {

    // Defining of Collections to be used across the whole app
    private UserAccount myData;
    private HashMap<String, UserAppData> appData;
    private UserAccount activeProfileView;
    private UserAppData activeProfile;

    public UserAppData getActiveProfile() {
        return activeProfile;
    }

    public void setActiveProfile(UserAppData activeProfile) {
        this.activeProfile = activeProfile;
    }

    public HashMap<String, UserAppData> getAppData() {
        return appData;
    }

    public void setAppData(HashMap<String, UserAppData> map)
    {
        this.appData = map;
    }


    public void setMyData(UserAccount data)
    {
        this.myData = data;
    }

    public UserAccount getMyData() {
        return myData;
    }


    public UserAccount getActiveProfileView() {
        return activeProfileView;
    }

    public void setActiveProfileView(UserAccount activeProfileView) {
        this.activeProfileView = activeProfileView;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Firebase.setAndroidContext(this);

        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
    }


}
