package com.project.android.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.project.android.R;
import com.project.android.classes.FilterResults;
import com.project.android.classes.Friends;
import com.project.android.classes.LARConnect;
import com.project.android.classes.UserAccount;
import com.project.android.classes.UserAppData;
import com.project.android.classes.UserLocation;
import com.skyfishjy.library.RippleBackground;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Cormac on 01/04/2017.
 */

public class LoadDataActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final int MY_PERMISSION_REQUEST_READ_FINE_LOCATION = 111;

    // Database References
    private DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("accounts");
    private FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

    // Object for user data and current user profile
    private HashMap<String, UserAppData> appData;
    private UserAccount myData;

    // background loading animation
    private RippleBackground rippleBackground;

    // async task counter int
    private int asyncTaskCount = 0;

    // radius for deciding what users are in range.
    private int radius = 50;

    private CustomApplication application;
    private GoogleApiClient mGoogleApiClient;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_loaddata);

        // Call to start progress animation and start loading data
        onAsyncTaskCompleteHandler(false);

        application = (CustomApplication) getApplication();
        appData = new HashMap<>();

        // listener to retrieve current users location proximity from Firebase

        mDatabase.child(user.getUid()).child("profile").child("proximityDist").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.getValue() != null) {
                    radius = dataSnapshot.getValue(Integer.class);
                }

                // initialise map object to get current users location for app.
                initialiseMapObject();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    // Connecting to Google Maps API
    public void initialiseMapObject() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mGoogleApiClient.connect();
    }

    // Method that handles data loaded completion
    private void onAsyncTaskCompleteHandler(boolean complete) {

        // if data not loaded, start animation
        if (!complete) {
            rippleBackground = (RippleBackground) findViewById(R.id.progress_animation);
            rippleBackground.startRippleAnimation();
        } else {

            // If data loaded, put map object through results filter
            // and larconnect, then load data into, Custom Application objects
            myData = appData.get(user.getUid()).getUser();
            appData.remove(user.getUid());

            if (appData.isEmpty() == false) {
                appData = FilterResults.filterResults(appData, myData);
                appData = (HashMap<String, UserAppData>) LARConnect.LARCSort(appData, myData, this);
            }

            application.setAppData(appData);
            application.setMyData(myData);

            // Start Main Activity.
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            rippleBackground.stopRippleAnimation();
            finish();
        }
    }

    // Method to get al User Data from Firebase Database
    private void getApplicationResourceData(final Location myLocation) {

        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                // cast result to a hashmap object
                HashMap<String, UserAccount> accounts;
                GenericTypeIndicator<HashMap<String, UserAccount>> dataType
                        = new GenericTypeIndicator<HashMap<String, UserAccount>>() {
                };

                accounts = dataSnapshot.getValue(dataType);


                if (accounts != null) {

                    for (String key : accounts.keySet()) {

                        // Create location object for a given user and check user is in range.
                        Location temp = new Location("");
                        temp.setLatitude(accounts.get(key).getProfile().getLocation().getLatitude());
                        temp.setLongitude(accounts.get(key).getProfile().getLocation().getLongitude());

                        int proximity = (int) Math.ceil(myLocation.distanceTo(temp) / 1000);

                        // Get facebook friends for users that are in range.
                        if (proximity <= radius) {
                            appData.put(key, new UserAppData(accounts.get(key), new ArrayList<UserAccount>(), new ArrayList<Friends>(), 0, 0, proximity, true, true));
                        }
                        else
                        {
                            appData.put(key, new UserAppData(accounts.get(key), new ArrayList<UserAccount>(), new ArrayList<Friends>(), 0, 0, proximity, false, false));
                        }

                        getFacebookMutualFriends(appData.get(key).getUser().getProfile().getSid(), key);
                    }
                } else {
                    onAsyncTaskCompleteHandler(true);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w("TAG", "loadPost:onCancelled", databaseError.toException());
            }
        });
    }

    // Facebook Api request to retrieve mutual friends
    private void getFacebookMutualFriends(String sid, final String key) {

        incrementAsyncTaskCount();
        Bundle params = new Bundle();
        params.putString("fields", "context.fields(mutual_friends)");
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/" + sid,
                params,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    @Override
                    public void onCompleted(GraphResponse graphResponse) {
                        try {
                            if (graphResponse.getRawResponse() != null) {
                                JSONObject jsonObject = new JSONObject(graphResponse.getRawResponse());
                                if (jsonObject.has("context")) {
                                    jsonObject = jsonObject.getJSONObject("context");
                                    if (jsonObject.has("mutual_friends")) {

                                        JSONArray mutualFriendsJSONArray = jsonObject.getJSONObject("mutual_friends").getJSONArray("data");

                                        // Load data into friend object of hashmap user app data
                                        appData.get(key).setFacebook_mutual(new ArrayList<Friends>());

                                        for (int i = 0; i < mutualFriendsJSONArray.length(); i++) {
                                            String mutual_sid = mutualFriendsJSONArray.getJSONObject(i).getString("id");
                                            String name = mutualFriendsJSONArray.getJSONObject(i).getString("name");


                                            appData.get(key).addItemToMutualList(new Friends(mutual_sid, name, null));

                                            getFacebookProfilePic(mutual_sid, key, i);
                                        }
                                    }
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                        decrementAsyncTaskCount();
                    }
                }
        ).executeAsync();
    }

    // Facebook Api to request profile image of users that are mutual friends
    private void getFacebookProfilePic(String sid, final String key, final int index) {

        incrementAsyncTaskCount();
        Bundle params = new Bundle();
        params.putString("fields", "picture.height(961)");
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/" + sid,
                params,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    @Override
                    public void onCompleted(GraphResponse graphResponse) {

                        // Porcessing of Facebook API response
                        try {
                            JSONObject jsonObject = new JSONObject(graphResponse.getRawResponse());

                            String url = jsonObject.getJSONObject("picture").getJSONObject("data").getString("url");

                            // Improving quality of Facebook photo
                            if (url.contains("_t.")) {
                                url = url.replaceAll("_t.", "_n.");
                            } else if (url.contains("_a.")) {
                                url = url.replaceAll("_a.", "_n.");
                            } else if (url.contains("_s.")) {
                                url = url.replaceAll("_s.", "_n.");
                            } else if (url.contains("_q.")) {
                                url = url.replaceAll("_q.", "_n.");
                            }

                            appData.get(key).getFacebook_mutual().get(index).setImageURL(url);

                        } catch (JSONException e) {

                            e.printStackTrace();
                        }
                        decrementAsyncTaskCount();
                    }
                }
        ).executeAsync();

    }

    // Check if all tasks are completed
    private void checkAsyncTaskFinish() {
        if (asyncTaskCount == 0) {
            onAsyncTaskCompleteHandler(true);
        }
    }

    // decrement task count
    private void decrementAsyncTaskCount() {
        asyncTaskCount--;
        checkAsyncTaskFinish();
    }

    // increment task count
    private void incrementAsyncTaskCount() {
        asyncTaskCount++;
    }

    // Location Permission checking and requesting
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location != null) {
            UserLocation loc = new UserLocation(location.getLatitude(), location.getLongitude());

            mDatabase.child(user.getUid()).child("profile").child("location").setValue(loc);
            getApplicationResourceData(location);
        }
    }

    // When connected to Google Maps API, request access to location and retrieve current users location and update on Firebase
    @Override
    public void onConnected(Bundle bundle) {


        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
            } else {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSION_REQUEST_READ_FINE_LOCATION);
                Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

                if (location != null) {
                    UserLocation loc = new UserLocation(location.getLatitude(), location.getLongitude());

                    mDatabase.child(user.getUid()).child("profile").child("location").setValue(loc);
                }
            }
        } else {
            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (location != null) {
                UserLocation loc = new UserLocation(location.getLatitude(), location.getLongitude());

                mDatabase.child(user.getUid()).child("profile").child("location").setValue(loc);
                getApplicationResourceData(location);
            }

        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}