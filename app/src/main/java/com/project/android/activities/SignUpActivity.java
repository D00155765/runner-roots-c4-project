package com.project.android.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.project.android.R;
import com.project.android.classes.MultiSelectionSpinner;
import com.project.android.classes.UserAccount;
import java.util.ArrayList;
import java.util.List;

import mehdi.sakout.fancybuttons.FancyButton;

import static android.text.TextUtils.concat;

/**
 * Created by Cormac on 01/04/2017.
 */

public class SignUpActivity extends AppCompatActivity {

    private DatabaseReference mRef = FirebaseDatabase.getInstance().getReference();

    // Declaration of variables
    private String country;
    private String day;
    private String month;
    private String year;
    private String blurb;
    private ArrayList<String> interests = new ArrayList<>();


    private EditText mEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        // Retrieving User Account object from Login Activity in Intent
        final UserAccount user_account = (UserAccount) getIntent().getSerializableExtra("account");

        // Material Spinners for Birthday
        MaterialSpinner spinnerDay = (MaterialSpinner) findViewById(R.id.day);
        spinnerDay.setDropdownMaxHeight(800);
        String[] days = getResources().getStringArray(R.array.day);
        spinnerDay.setItems(days);
        spinnerDay.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item)
            {
                day = item;
            }
        });

        MaterialSpinner spinnerMonth = (MaterialSpinner) findViewById(R.id.month);
        spinnerMonth.setDropdownMaxHeight(800);
        String[] months = getResources().getStringArray(R.array.month);
        spinnerMonth.setItems(months);
        spinnerMonth.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                month = item;
            }
        });

        MaterialSpinner spinnerYear = (MaterialSpinner) findViewById(R.id.year);
        spinnerYear.setDropdownMaxHeight(800);
        String[] years = getResources().getStringArray(R.array.year);
        spinnerYear.setItems(years);
        spinnerYear.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                year = item;
            }
        });

        // Multi Selection Spinner for Interests
        String[] array = getResources().getStringArray(R.array.interests_array);
        MultiSelectionSpinner multiSelectionSpinner = (MultiSelectionSpinner) findViewById(R.id.mySpinner);
        multiSelectionSpinner.setItems(array);
        multiSelectionSpinner.setListener(new MultiSelectionSpinner.OnMultipleItemsSelectedListener() {
            @Override
            public void selectedIndices(List<Integer> indices) {
            }

            @Override
            public void selectedStrings(List<String> strings) {
                interests.addAll(strings);
            }
        });

        final AutoCompleteTextView textView;
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_dropdown_item,
                getResources().getStringArray(R.array.countries_array));


        // TextView for Country List Selection
        textView = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);
        textView.setAdapter(arrayAdapter);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View arg0) {
                textView.showDropDown();
            }
        });

        // Blurb EditText Field
        mEdit = (EditText)findViewById(R.id.blurb);

        // Submit Button
        FancyButton button = (FancyButton) findViewById(R.id.click_done);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click

                String dob = day + "/" + concat(month) + "/" + concat(year);
                blurb = mEdit.getText().toString();
                country = textView.getText().toString();
                user_account.getProfile().setInterests(interests);
                user_account.getProfile().setBlurb(blurb);
                user_account.getProfile().setCountry(country);
                user_account.getProfile().setDob(dob);



                // Listener to see if account was Created
                mRef.child("accounts").addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                        // Start LoadDataActivity
                        Intent intent = new Intent(getApplicationContext(), LoadDataActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                mRef.child("accounts").child(user_account.getProfile().getUid()).setValue(user_account);
                finish();
            }
        });
    }
}

