package com.project.android.activities;

import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.project.android.R;
import com.project.android.classes.Message;
import com.project.android.classes.UserAccount;
import com.project.android.classes.UserProfile;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

import static java.security.AccessController.getContext;

/**
 * Created by Cormac on 30/04/2017.
 */

public class ChatActivity extends AppCompatActivity {

    // Variables to reference views in this activities layout file.
    private LinearLayout layout;
    private ImageView sendButton;
    private EditText messageArea;
    private ScrollView scrollView;

    // Custom Application variables
    private UserProfile profile;
    private UserAccount myData;
    private CustomApplication application;

    // Toolbar at top of chat activity
    private Toolbar toolbar;

    // Declaring of fonts
    private Typeface mainFont;
    private Typeface shadeFont;

    // Firebase references to database and user session token
    private DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
    private FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.active_chat_activity);

        // Instantiating of layout views
        layout = (LinearLayout) findViewById(R.id.layout1);
        sendButton = (ImageView) findViewById(R.id.sendButton);
        messageArea = (EditText) findViewById(R.id.messageArea);
        scrollView = (ScrollView) findViewById(R.id.scrollView);

        // font instantiation
        mainFont = Typeface.createFromAsset(getApplication().getAssets(), "fonts/Titillium-RegularUpright.otf");
        shadeFont = Typeface.createFromAsset(getApplication().getAssets(), "fonts/Roboto-Light.ttf");

        // instantiation of app data.
        application = (CustomApplication) getApplication();
        myData = application.getMyData();

        // receiving intent from messenger fragment
        profile = (UserProfile) getIntent().getSerializableExtra("profile");

        // instantiating toolbar and adding text and user profile image.
        toolbar = (Toolbar) findViewById(R.id.chat_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");

        CircleImageView img = (CircleImageView) toolbar.findViewById(R.id.toolBarImg);
        Glide.with(getApplicationContext()).load(profile.getImageURL()).into(img);

        TextView txt = (TextView) toolbar.findViewById(R.id.titleTxt);
        txt.setText(profile.getFname() + ", " + profile.getCountry());
        txt.setTypeface(mainFont);

        // making scrollview stay scrolled to bottom of screen
        scrollView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                scrollView.post(new Runnable() {
                    public void run() {
                        scrollView.fullScroll(View.FOCUS_DOWN);
                    }
                });
            }
        });


        // Firebase listener that listens at the specified child node of current user,
        // and then the node equal to the selected users UID.
        mDatabase.child("messages").child(user.getUid()).child(profile.getUid()).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                // Define message object based on listener result.
                String key = dataSnapshot.getKey();
                Message msg = dataSnapshot.getValue(Message.class);

                // sending message to method to add message box view to layout.
                // If 1, message is from active user, if 2, message is from another user.
                if (!key.equals("last_message")) {
                    if (msg.getFrom().equals(user.getUid())) {
                        addMessageBox(msg, 1);
                    } else {
                        addMessageBox(msg, 2);
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        // on click of send button, push message object to database to be pulled by other user.
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String messageText = messageArea.getText().toString();

                if(messageText .length() > 0) {
                    messageArea.getText().clear();

                    String timestamp = Long.toString(System.currentTimeMillis());
                    Message msg = new Message(user.getUid(), profile.getUid(), messageText, timestamp);


                    mDatabase.child("messages").child(user.getUid()).child(profile.getUid()).child("last_message").setValue(msg);
                    mDatabase.child("messages").child(profile.getUid()).child(user.getUid()).child("last_message").setValue(msg);
                    mDatabase.child("messages").child(user.getUid()).child(profile.getUid()).push().setValue(msg);
                    mDatabase.child("messages").child(profile.getUid()).child(user.getUid()).push().setValue(msg);
                }
            }
        });
    }

    //    add Message to Chat View Method.
    //    In this method, application views are created for a textview to
    //    hold the message, a image view to hold picture of message sender, and a text view to hold a timestamp.
    // These views are all put into linear layout messageWrapper
    public void addMessageBox(Message messageObj, int type) {

        String relTimestamp = getRelevantTime(messageObj.getTimestamp());
        String message = messageObj.getMessage();


        LinearLayout.LayoutParams timestampParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        timestampParams.setMargins(5, 5, 5, 20);

        TextView timestampTxt = new TextView(ChatActivity.this);
        timestampTxt.setLayoutParams(timestampParams);
        timestampTxt.setText(relTimestamp);
        timestampTxt.setGravity(Gravity.CENTER_HORIZONTAL);
        timestampTxt.setTextSize(12);
        timestampTxt.setTypeface(mainFont);

        TextView textView = new TextView(ChatActivity.this);
        LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        textParams.setMargins(50, 15, 50, 0);
        textView.setLayoutParams(textParams);
        textView.setText(message);
        textView.setMaxWidth(900);
        textView.setTextSize(17);
        textView.setGravity(Gravity.CENTER_VERTICAL);
        textView.setTypeface(shadeFont);

        CircleImageView img = new CircleImageView(ChatActivity.this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(150, 150);
        img.setLayoutParams(layoutParams);
        Glide.with(getApplicationContext()).load(profile.getImageURL()).into(img);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(40, 20, 40, 0);

        LinearLayout messageWrapper = new LinearLayout(ChatActivity.this);
        messageWrapper.setLayoutParams(params);
        messageWrapper.setOrientation(LinearLayout.HORIZONTAL);

        if (type == 1) {
            Glide.with(getApplicationContext()).load(myData.getProfile().getImageURL()).into(img);
            messageWrapper.setHorizontalGravity(Gravity.RIGHT);
            textView.setBackgroundResource(R.drawable.rounded_corner1);
            textView.setTextColor(Color.WHITE);
            messageWrapper.addView(textView);
            messageWrapper.addView(img);

        } else {
            Glide.with(getApplicationContext()).load(profile.getImageURL()).into(img);
            messageWrapper.setHorizontalGravity(Gravity.LEFT);
            textView.setBackgroundResource(R.drawable.rounded_corner2);
            textView.setTextColor(Color.BLACK);
            messageWrapper.addView(img);
            messageWrapper.addView(textView);
        }

        layout.addView(messageWrapper);
        layout.addView(timestampTxt);
    }

    // method to format long time into a user readible time format

    public String getRelevantTime(String timestamp) {
        String relTime = timestamp;
        Long longDate = Long.valueOf(relTime);

        Calendar cal = Calendar.getInstance();
        Date da = new Date(longDate);
        cal.setTime(da);


        if (DateUtils.isToday(longDate)) {
            relTime = DateFormat.getTimeInstance(DateFormat.SHORT).format(da);
        } else {
            relTime = DateFormat.getDateInstance(DateFormat.MEDIUM).format(da);
            relTime = relTime.substring(0, relTime.length() - 4);
        }
        return relTime;
    }

}