package com.project.android.fragments;


import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.login.widget.ProfilePictureView;
import com.google.firebase.auth.FirebaseUser;
import com.project.android.R;
import com.project.android.activities.CustomApplication;
import com.project.android.classes.UserAccount;
import com.project.android.classes.UserAppData;

/**
 * Created by Cormac on 26/03/2017.
 */

public class ProfileInfoFragment extends Fragment {

    // Declaration of variables
    private CustomApplication application;
    private TextView name;
    private FirebaseUser user;
    private ProfilePictureView profilePictureView;

    public ProfileInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        application = (CustomApplication) getActivity().getApplication();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.profileinfo_fragment, container, false);

        // Font Setup
        Typeface mainFont = Typeface.createFromAsset(getContext().getAssets(), "fonts/Titillium-RegularUpright.otf");
        UserAppData profile = application.getActiveProfile();

        // Loading all data into TextViews in Fragment
        TextView score = (TextView) rootView.findViewById(R.id.score_num);
        score.setText(Integer.toString(profile.getScore()) + "% Match");
        score.setTypeface(mainFont);

        TextView proximity = (TextView) rootView.findViewById(R.id.proximity);
        proximity.setText(Integer.toString(profile.getProximity()) + "km Away");
        proximity.setTypeface(mainFont);

        TextView age = (TextView) rootView.findViewById(R.id.age);
        age.setText(Integer.toString(profile.getUser().getProfile().getAge()) + " years old");
        age.setTypeface(mainFont);

        TextView country = (TextView) rootView.findViewById(R.id.country);
        country.setText("From " + profile.getUser().getProfile().getCountry());
        country.setTypeface(mainFont);

        TextView blurb = (TextView) rootView.findViewById(R.id.blurb);
        blurb.setText(profile.getUser().getProfile().getBlurb());
        blurb.setTypeface(mainFont);

        TextView interests = (TextView) rootView.findViewById(R.id.interests);
        interests.setText(profile.getUser().getProfile().getInterestString(profile.getUser().getProfile().getInterests()));
        interests.setTypeface(mainFont);

        return rootView;
    }
}