package com.project.android.fragments;


import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.project.android.R;
import com.project.android.activities.CustomApplication;
import com.project.android.adapters.AppLocalMutualGridAdapter;
import com.project.android.adapters.FacebookMutualGridAdapter;
import com.project.android.classes.Friends;
import com.project.android.classes.UserAccount;
import com.project.android.classes.UserAppData;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Cormac on 26/03/2017.
 */

public class ProfileConnectionFragment extends Fragment {

    // Declaration of GridViews
    private CustomApplication application;
    private GridView facebookList;
    private GridView appLocalList;

    public ProfileConnectionFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        application = (CustomApplication) getActivity().getApplication();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.profileconnection_fragment, container, false);

        // Setting up data collections
        UserAppData profile = application.getActiveProfile();
        HashMap<String, UserAppData> map = application.getAppData();

        //instantiate GridViews
        facebookList = (GridView) rootView.findViewById(R.id.facebookGridView);
        appLocalList = (GridView) rootView.findViewById(R.id.appLocalGridView);


        // Loading data into facebook grid adapters
        if (profile.getFacebook_mutual() != null)
        {
            FacebookMutualGridAdapter facebookAdapter=new FacebookMutualGridAdapter(getContext(),R.layout.profileconnection_fragment, profile.getFacebook_mutual());
            facebookList.setAdapter(facebookAdapter);
        }

        // Loading data into local app mutuals grid adapters
        if (profile.getLocal_mutual() != null)
        {
            AppLocalMutualGridAdapter appLocalAdapter=new AppLocalMutualGridAdapter(getContext(),R.layout.profileconnection_fragment, profile.getLocal_mutual());
            appLocalList.setAdapter(appLocalAdapter);
        }

        // Floating action menu Button setup
        final FloatingActionsMenu menuMultipleActions = (FloatingActionsMenu) rootView.findViewById(R.id.multiple_actions);
        final FloatingActionButton facebook_mutual_btn = (FloatingActionButton) rootView.findViewById(R.id.facebook_mutual_btn);

        // Facebook FAB Btn clickListener
        facebook_mutual_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                appLocalList.setVisibility(View.GONE);
                facebookList.setVisibility(View.VISIBLE);

                rootView.findViewById(R.id.emptyView).setVisibility(View.GONE);
                facebookList.setEmptyView(rootView.findViewById(R.id.emptyView));

                menuMultipleActions.collapse();
            }
        });

        //App Local FAB Btn clickListener
        final FloatingActionButton app_local_btn = (FloatingActionButton) rootView.findViewById(R.id.app_local_btn);
        app_local_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                facebookList.setVisibility(View.GONE);
                appLocalList.setVisibility(View.VISIBLE);

                rootView.findViewById(R.id.emptyView).setVisibility(View.GONE);
                appLocalList.setEmptyView(rootView.findViewById(R.id.emptyView));

                menuMultipleActions.collapse();
            }
        });

        return rootView;
    }
}
