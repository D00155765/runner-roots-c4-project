package com.project.android.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.project.android.R;
import com.project.android.activities.ChatActivity;
import com.project.android.classes.ChatItem;
import com.project.android.adapters.ChatItemAdapter;
import com.project.android.classes.Message;
import com.project.android.classes.UserAccount;
import com.project.android.classes.UserProfile;

/**
 * Created by Cormac on 26/03/2017.
 */

public class MessagingFragment extends Fragment {

    //Initialising of Database References
    private DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
    private FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

    // Listview and adapter for ChatItems
    private ListView activeChats;
    private ChatItemAdapter activeChatAdapter;

    //HashMaop for holding ChatItem Data
    private HashMap<String, UserProfile> userMap = new HashMap<>();

    public MessagingFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.messaging_fragment, container, false);

        // Declaring of Listview and adapter of Chat Items
        activeChats = (ListView) rootView.findViewById(R.id.active_chats);
        activeChats.setEmptyView(rootView.findViewById(R.id.emptyView));
        activeChatAdapter = new ChatItemAdapter(getActivity(), R.layout.chat_item, new ArrayList<ChatItem>());
        activeChats.setAdapter(activeChatAdapter);

        // Item OnCLick Listener
        activeChats.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getContext(), ChatActivity.class);
                intent.putExtra("profile", userMap.get(activeChatAdapter.getItem(position).getUid()));
                startActivity(intent);
            }
        });

        // Child node listener to retriev active users chats and last message data
        mDatabase.child("messages").child(user.getUid())
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        HashMap<String, Message> map;

                        GenericTypeIndicator<HashMap<String, Message>> dataType
                                = new GenericTypeIndicator<HashMap<String, Message>>() {
                        };

                        final String userRefKey = dataSnapshot.getKey();
                        map = dataSnapshot.getValue(dataType);

                        String last_message = "";
                        String timestamp = "";

                        if (map.containsKey("last_message")) {
                            last_message = map.get("last_message").getMessage();
                            timestamp = getRelevantTime(map.get("last_message").getTimestamp());
                        }

                        final String finalLast_message = last_message;
                        final String finalTimestamp = timestamp;

                        mDatabase.child("accounts").child(userRefKey)
                                .addListenerForSingleValueEvent(new ValueEventListener() {

                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {

                                        // Retrieving the UserAccount of the User that is in chat
                                        UserAccount profile = dataSnapshot.getValue(UserAccount.class);
                                        userMap.put(userRefKey, profile.getProfile());

                                        // Create new Chat Item
                                        ChatItem item = new ChatItem(profile.getProfile().getUid(), profile.getProfile()
                                                .getImageURL(), profile.getProfile().getFname(),
                                                profile.getProfile().getCountry(), finalLast_message, finalTimestamp);

                                        // Add Item to Adapter and Notify Changes
                                        activeChatAdapter.add(item);
                                        activeChatAdapter.notifyDataSetChanged();
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                    }
                                });
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                        // Checks for changes in last message node to update message and Timestamp
                        GenericTypeIndicator<HashMap<String, Message>> dataType
                                = new GenericTypeIndicator<HashMap<String, Message>>() {
                        };

                        final String userRefKey = dataSnapshot.getKey();
                        HashMap<String, Message> map = dataSnapshot.getValue(dataType);

                        String last_message = "";
                        String timestamp = "";

                        if (map.containsKey("last_message")) {
                            last_message = map.get("last_message").getMessage();
                            timestamp = getRelevantTime(map.get("last_message").getTimestamp());
                        }

                        final String finalLast_message = last_message;
                        final String finalTimestamp = timestamp;

                        for(int i = 0; i < activeChatAdapter.getCount(); i++)
                        {
                            if(userRefKey.equals(activeChatAdapter.getItem(i).getUid()))
                            {
                                activeChatAdapter.getItem(i).setLast_message(finalLast_message);
                                activeChatAdapter.getItem(i).setDate(finalTimestamp);
                                activeChatAdapter.notifyDataSetChanged();
                            }
                        }
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });

        return rootView;
    }

    // Gets relevant human readable time of time in Long seconds
    public String getRelevantTime(String timestamp) {
        String relTime = timestamp;
        Long longDate = Long.valueOf(relTime);

        Calendar cal = Calendar.getInstance();
        Date da = new Date(longDate);
        cal.setTime(da);

        if (DateUtils.isToday(longDate)) {
            relTime = DateFormat.getTimeInstance(DateFormat.SHORT).format(da);
        } else {
            relTime = DateFormat.getDateInstance(DateFormat.MEDIUM).format(da);
            relTime = relTime.substring(0, relTime.length() - 4);
        }
        return relTime;
    }
}