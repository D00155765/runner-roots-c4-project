package com.project.android.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.cuboid.cuboidcirclebutton.CuboidButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mindorks.placeholderview.SwipeDecor;
import com.mindorks.placeholderview.SwipePlaceHolderView;
import com.project.android.R;
import com.project.android.activities.CustomApplication;
import com.project.android.activities.ProfileViewActivity;
import com.project.android.classes.TinderCard;
import com.project.android.classes.UserAppData;

import java.util.HashMap;

/**
 * Created by Cormac on 26/03/2017.
 */

public class LARConnectFragment extends Fragment {
    // Declaring of views - TinderCard View and Context
    private SwipePlaceHolderView mSwipeView;
    private Context mContext;

    // App database and data references
    private DatabaseReference mDatabase;
    private HashMap<String, UserAppData> appData;
    private CustomApplication application;

    public LARConnectFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        application = (CustomApplication) getActivity().getApplication();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Initialise Views
        View rootView = inflater.inflate(R.layout.larconnect_fragment, container, false);

        mSwipeView = (SwipePlaceHolderView) rootView.findViewById(R.id.swipeView);
        mContext = getContext();

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.keepSynced(true);

        // Build Tinder Card Swipe View
        buildSwipeView();

        // Loop through map of users and add cardview for each user.
        appData = application.getAppData();

        for (String key : appData.keySet()) {

            if(appData.get(key).isInRange()) {
                mSwipeView.addView(new TinderCard(mContext, appData.get(key), mSwipeView));
            }
        }

        // Buttons to connect or reject a user
        rootView.findViewById(R.id.acceptBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSwipeView.doSwipe(false);
            }
        });

        rootView.findViewById(R.id.rejectBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSwipeView.doSwipe(true);
            }
        });

        return rootView;
    }


    //   Source "https://github.com/janishar/PlaceHolderView" Accessed "12/03/17"
    public Point getDisplaySize(WindowManager windowManager) {
        try {
            if (Build.VERSION.SDK_INT > 16) {
                Display display = windowManager.getDefaultDisplay();
                DisplayMetrics displayMetrics = new DisplayMetrics();
                display.getMetrics(displayMetrics);
                return new Point(displayMetrics.widthPixels, displayMetrics.heightPixels);
            } else {
                return new Point(0, 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new Point(0, 0);
        }
    }

    //   Source "https://github.com/janishar/PlaceHolderView" Accessed "12/03/17"
    public int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    //   Source "https://github.com/janishar/PlaceHolderView" Accessed "12/03/17"
    public void buildSwipeView() {

        int bottomMargin = dpToPx(160);
        Point windowSize = getDisplaySize((WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE));

        mSwipeView.getBuilder()
                .setSwipeType(SwipePlaceHolderView.SWIPE_TYPE_VERTICAL)
                .setDisplayViewCount(3)
                .setHeightSwipeDistFactor(15)
                .setWidthSwipeDistFactor(15)
                .setSwipeDecor(new SwipeDecor()
                        .setViewWidth(windowSize.x - 30)
                        .setViewHeight(windowSize.y - bottomMargin - 10)
                        .setViewGravity(Gravity.CENTER_HORIZONTAL)
                        .setPaddingTop(20)
                        .setRelativeScale(0.01f)
                        .setSwipeInMsgLayoutId(R.layout.tinder_swipe_in_msg_view)
                        .setSwipeOutMsgLayoutId(R.layout.tinder_swipe_out_msg_view));

    }
}