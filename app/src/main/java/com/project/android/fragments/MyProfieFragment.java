package com.project.android.fragments;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cuboid.cuboidcirclebutton.CuboidButton;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.widget.ProfilePictureView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.project.android.R;
import com.project.android.activities.CustomApplication;
import com.project.android.activities.LoadDataActivity;
import com.project.android.activities.LoginActivity;
import com.project.android.activities.ProfileViewActivity;
import com.project.android.activities.SignUpActivity;
import com.project.android.classes.Friends;
import com.project.android.classes.UserAccount;
import com.project.android.classes.UserProfile;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by Cormac on 26/03/2017.
 */

public class MyProfieFragment extends Fragment
{
    // Declaration of activities variables
    private CustomApplication application;
    private TextView name;
    private TextView info_details;
    private FirebaseUser user;
    private DatabaseReference accountRef = FirebaseDatabase.getInstance().getReference().child("accounts");

    private Typeface font;
    private CircleImageView profile_pic;

    public MyProfieFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        application = (CustomApplication) getActivity().getApplication();
        font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Titillium-RegularUpright.otf");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.myprofile_fragment, container, false);

        user = FirebaseAuth.getInstance().getCurrentUser();

        // Instantiation and Loading of data into layout views.

        // Profile Pic
        profile_pic = (CircleImageView) rootView.findViewById(R.id.profile_image);
        Glide.with(getContext()).load(application.getMyData().getProfile().getImageURL()).into(profile_pic);

        // Users name
        name = (TextView) rootView.findViewById(R.id.user_profile_name);
        name.setText(application.getMyData().getProfile().getName());
        name.setTypeface(font);

        // Users age and country
        info_details = (TextView) rootView.findViewById(R.id.info_details);
        info_details.setText(application.getMyData().getProfile().getAge() + ", " + application.getMyData().getProfile().getCountry() );
        info_details.setTypeface(font);


        // Logout Button Click Listener

        rootView.findViewById(R.id.logout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                LoginManager.getInstance().logOut();

                Intent intent = new Intent(getContext(), LoginActivity.class);
                startActivity(intent);

                getActivity().finish();
            }
        });

        // Update Facebook Button Click Listener

        rootView.findViewById(R.id.update_profile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        try {
                            Log.i("HERE", "HERE");
                            int age = object.getJSONObject("age_range").getInt("min");
                            String gender = object.getString("gender");
                            String imageURL = object.getJSONObject("picture").getJSONObject("data").getString("url");

                            if (imageURL.contains("_t.")) {
                                imageURL = imageURL.replaceAll("_t.", "_n.");
                            } else if (imageURL.contains("_a.")) {
                                imageURL = imageURL.replaceAll("_a.", "_n.");
                            } else if (imageURL.contains("_s.")) {
                                imageURL = imageURL.replaceAll("_s.", "_n.");
                            } else if (imageURL.contains("_q.")) {
                                imageURL = imageURL.replaceAll("_q.", "_n.");
                            }


                            accountRef.child(user.getUid()).child("profile").child("name").setValue(Profile.getCurrentProfile().getName());
                            accountRef.child(user.getUid()).child("profile").child("fname").setValue(Profile.getCurrentProfile().getFirstName());
                            accountRef.child(user.getUid()).child("profile").child("sid").setValue(Profile.getCurrentProfile().getId());

                            accountRef.child(user.getUid()).child("profile").child("gender").setValue(gender);
                            accountRef.child(user.getUid()).child("profile").child("age").setValue(age);
                            accountRef.child(user.getUid()).child("profile").child("imageURL").setValue(imageURL);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "gender, age_range, picture.height(961)");
                request.setParameters(parameters);
                request.executeAsync();
            }
        });

        return rootView;
    }
}
