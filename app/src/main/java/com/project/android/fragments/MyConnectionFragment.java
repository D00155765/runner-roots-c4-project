package com.project.android.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.project.android.R;
import com.project.android.activities.CustomApplication;
import com.project.android.activities.ProfileViewActivity;
import com.project.android.adapters.ConnectionAdapter;
import com.project.android.adapters.FriendAdapter;
import com.project.android.adapters.FriendRequestAdapter;
import com.project.android.classes.UserAccount;
import com.project.android.adapters.ConnectRequestAdapter;

import java.util.ArrayList;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by Cormac on 26/03/2017.
 */

public class MyConnectionFragment extends Fragment {

    private CustomApplication application;

    // Declaring of four adapters
    private ConnectRequestAdapter connectRequestAdapter;
    private FriendRequestAdapter friendRequestAdapter;
    private ConnectionAdapter connectionAdapter;
    private FriendAdapter friendAdapter;

    // Declaring of four Listviews
    private ListView friendList;
    private ListView connectionList;
    private ListView friendRequestList;
    private ListView connectRequestList;

    // Declaring of Database References
    private DatabaseReference mDatabase;
    private FirebaseUser user;

    public MyConnectionFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        application = (CustomApplication) getActivity().getApplication();
        user = FirebaseAuth.getInstance().getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.myconnection_fragment, container, false);

        // Initialisation of all the Listviews and Adapters
        connectRequestList = (ListView) rootView.findViewById(R.id.connectRequests);
        connectRequestAdapter = new ConnectRequestAdapter(getApplicationContext(), R.layout.connect_request_item, new ArrayList<UserAccount>());
        connectRequestList.setAdapter(connectRequestAdapter);

        friendRequestList = (ListView) rootView.findViewById(R.id.friendRequests);
        friendRequestAdapter = new FriendRequestAdapter(getApplicationContext(), R.layout.friend_request_item, new ArrayList<UserAccount>());
        friendRequestList.setAdapter(friendRequestAdapter);

        connectionList = (ListView) rootView.findViewById(R.id.connections);
        connectionAdapter = new ConnectionAdapter(getActivity(), R.layout.connection_item, new ArrayList<UserAccount>());
        connectionList.setAdapter(connectionAdapter);

        friendList = (ListView) rootView.findViewById(R.id.friends);
        friendAdapter = new FriendAdapter(getActivity(), R.layout.friend_item, new ArrayList<UserAccount>());
        friendList.setAdapter(friendAdapter);

        connectionList.setVisibility(View.GONE);
        friendRequestList.setVisibility(View.GONE);
        friendList.setVisibility(View.GONE);
        connectRequestList.setVisibility(View.VISIBLE);
        connectRequestList.setEmptyView(rootView.findViewById(R.id.emptyView));


        // A group of four listeners to fill and update the adapters and listviews with reqquests, connections and friends objects.
        mDatabase.child("accounts").child(user.getUid()).child("connectionRequests")
                    .addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                            String userRefKey = dataSnapshot.getKey();
                            UserAccount acc = dataSnapshot.getValue(UserAccount.class);

                            connectRequestAdapter.add(acc);

                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {
                            String userRefKey = dataSnapshot.getKey();
                            UserAccount acc = dataSnapshot.getValue(UserAccount.class);

                            for(int i = 0; i < connectRequestAdapter.getCount(); i++) {
                                if (userRefKey.equals(connectRequestAdapter.getItem(i).getProfile().getUid())) {
                                    connectRequestAdapter.remove(connectRequestAdapter.getItem(i));
                                    connectRequestAdapter.notifyDataSetChanged();
                                }
                            }
                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

        mDatabase.child("accounts").child(user.getUid()).child("friendRequests")
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        String userRefKey = dataSnapshot.getKey();
                        UserAccount acc = dataSnapshot.getValue(UserAccount.class);

                        friendRequestAdapter.add(acc);
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        String userRefKey = dataSnapshot.getKey();
                        UserAccount acc = dataSnapshot.getValue(UserAccount.class);

                        for(int i = 0; i < friendRequestAdapter.getCount(); i++) {
                            if (userRefKey.equals(friendRequestAdapter.getItem(i).getProfile().getUid())) {
                                friendRequestAdapter.remove(friendRequestAdapter.getItem(i));
                                friendRequestAdapter.notifyDataSetChanged();
                            }
                        }
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

        mDatabase.child("accounts").child(user.getUid()).child("connections")
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        String userRefKey = dataSnapshot.getKey();
                        UserAccount acc = dataSnapshot.getValue(UserAccount.class);

                        connectionAdapter.add(acc);
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        String userRefKey = dataSnapshot.getKey();
                        UserAccount acc = dataSnapshot.getValue(UserAccount.class);

                        for(int i = 0; i < connectionAdapter.getCount(); i++) {
                            if (userRefKey.equals(connectionAdapter.getItem(i).getProfile().getUid())) {
                                connectionAdapter.remove(connectionAdapter.getItem(i));
                                connectionAdapter.notifyDataSetChanged();
                            }
                        }

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

        mDatabase.child("accounts").child(user.getUid()).child("friends")
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        String userRefKey = dataSnapshot.getKey();
                        UserAccount acc = dataSnapshot.getValue(UserAccount.class);

                        friendAdapter.add(acc);
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        String userRefKey = dataSnapshot.getKey();
                        UserAccount acc = dataSnapshot.getValue(UserAccount.class);

                        for(int i = 0; i < friendAdapter.getCount(); i++) {
                            if (userRefKey.equals(friendAdapter.getItem(i).getProfile().getUid())) {
                                friendAdapter.remove(friendAdapter.getItem(i));
                                friendAdapter.notifyDataSetChanged();
                            }
                        }
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

        // Floating action Menu and Buttons to handle button clicks
        // Controlling Visibility of List views based on what items are clicked in teh FAB Button.

        final FloatingActionsMenu menuMultipleActions = (FloatingActionsMenu) rootView.findViewById(R.id.multiple_actions);

        final FloatingActionButton action_friend_requests = (FloatingActionButton) rootView.findViewById(R.id.action_friend_requests);
        action_friend_requests.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                friendList.setVisibility(View.GONE);
                connectionList.setVisibility(View.GONE);
                connectRequestList.setVisibility(View.GONE);
                friendRequestList.setVisibility(View.VISIBLE);

                rootView.findViewById(R.id.emptyView).setVisibility(View.GONE);
                friendRequestList.setEmptyView(rootView.findViewById(R.id.emptyView));

                menuMultipleActions.collapse();
            }
        });

        final FloatingActionButton action_connect_requests = (FloatingActionButton) rootView.findViewById(R.id.action_connect_requests);
        action_connect_requests.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                friendList.setVisibility(View.GONE);
                connectionList.setVisibility(View.GONE);
                friendRequestList.setVisibility(View.GONE);
                connectRequestList.setVisibility(View.VISIBLE);

                rootView.findViewById(R.id.emptyView).setVisibility(View.GONE);
                connectRequestList.setEmptyView(rootView.findViewById(R.id.emptyView));

                menuMultipleActions.collapse();
            }
        });

        final FloatingActionButton action_connections = (FloatingActionButton) rootView.findViewById(R.id.action_connections);
        action_connections.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                friendList.setVisibility(View.GONE);
                connectRequestList.setVisibility(View.GONE);
                friendRequestList.setVisibility(View.GONE);
                connectionList.setVisibility(View.VISIBLE);

                rootView.findViewById(R.id.emptyView).setVisibility(View.GONE);
                connectionList.setEmptyView(rootView.findViewById(R.id.emptyView));

                menuMultipleActions.collapse();
            }
        });

        final FloatingActionButton action_friends = (FloatingActionButton) rootView.findViewById(R.id.action_friends);
        action_friends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                connectionList.setVisibility(View.GONE);
                connectRequestList.setVisibility(View.GONE);
                friendRequestList.setVisibility(View.GONE);
                friendList.setVisibility(View.VISIBLE);

                rootView.findViewById(R.id.emptyView).setVisibility(View.GONE);
                friendList.setEmptyView(rootView.findViewById(R.id.emptyView));

                menuMultipleActions.collapse();
            }
        });


        return rootView;
    }

}
