package com.project.android.fragments;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.project.android.R;
import com.project.android.activities.CustomApplication;
import com.project.android.activities.LoadDataActivity;
import com.project.android.classes.UserAppData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * Created by Cormac on 26/03/2017.
 */

public class ProximityMapFragment extends Fragment implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        OnMapReadyCallback,
        LocationListener {

    // Declaration of variables
    private int radius = 50000;
    private int beforeRad = 50000;

    public static final String TAG = ProximityMapFragment.class.getSimpleName();
    private static final int MY_PERMISSION_REQUEST_READ_FINE_LOCATION = 111;

    // Database References
    private DatabaseReference proximtyRef = FirebaseDatabase.getInstance().getReference().child("accounts");
    private FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

    // Map Object References
    private GoogleMap mMap;
    private MapView mapView;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private LatLng tempLatLng;

    // Data Loading Objects
    CustomApplication application;
    HashMap<String, UserAppData> map = new HashMap<>();

    public ProximityMapFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        application = (CustomApplication) getActivity().getApplication();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.googlemap_fragment, container, false);

        // Font setup
        Typeface mainFont = Typeface.createFromAsset(getContext().getAssets(), "fonts/Titillium-RegularUpright.otf");

        // Proximity Bar at top of view
        TextView proximity_display = (TextView) rootView.findViewById(R.id.display);
        proximity_display.setTypeface(mainFont);

        map = application.getAppData();
        mapView = (MapView) rootView.findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);


        // Requesting data proximity of user from Firebase
        proximtyRef.child(user.getUid()).child("profile").child("proximityDist").addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if(dataSnapshot.getValue() != null) {
                    radius = dataSnapshot.getValue(Integer.class) * 1000;
                    TextView txt = (TextView) rootView.findViewById(R.id.display);
                    txt.setText(radius/1000 + "km");
                }

                initialiseMapObject();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });



        // Floating Action Button Setup and Listeners

        // Zoom In Btn
        final FloatingActionButton action_zoom_in = (FloatingActionButton) rootView.findViewById(R.id.action_zoom_in);
        action_zoom_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView txt = (TextView) rootView.findViewById(R.id.display);

                String str = txt.getText().toString().replace("km", "");
                int num = Integer.valueOf(str);

                if (num == 50) {
                    txt.setText(Integer.toString(100) + "km");
                    radius += 50000;
                    initCamera(tempLatLng);
                } else if (num == 25) {
                    txt.setText(Integer.toString(50) + "km");
                    radius += 25000;
                    initCamera(tempLatLng);
                } else if (num == 10) {
                    txt.setText(Integer.toString(25) + "km");
                    radius += 15000;
                    initCamera(tempLatLng);
                } else if (num == 5) {
                    txt.setText(Integer.toString(10) + "km");
                    radius += 5000;
                    initCamera(tempLatLng);
                } else if (num == 1) {
                    txt.setText(Integer.toString(5) + "km");
                    radius += 4000;
                    initCamera(tempLatLng);
                }

            }
        });

        // Zoom Out Btn
        final FloatingActionButton action_connections = (FloatingActionButton) rootView.findViewById(R.id.action_zoom_out);
        action_connections.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView txt = (TextView) rootView.findViewById(R.id.display);
                String str = txt.getText().toString().replace("km", "");
                int num = Integer.valueOf(str);

                if (num == 50) {
                    txt.setText(Integer.toString(25) + "km");
                    radius -= 25000;
                    initCamera(tempLatLng);
                } else if (num == 25) {
                    txt.setText(Integer.toString(10) + "km");
                    radius -= 15000;
                    initCamera(tempLatLng);
                } else if (num == 10) {
                    txt.setText(Integer.toString(5) + "km");
                    radius -= 5000;
                    initCamera(tempLatLng);
                } else if (num == 5) {
                    txt.setText(Integer.toString(1) + "km");
                    radius -= 4000;
                    initCamera(tempLatLng);
                } else if (num == 100) {
                    txt.setText(Integer.toString(50) + "km");
                    radius -= 50000;
                    initCamera(tempLatLng);
                }


            }
        });

        // Map Legend Btn
        final FloatingActionsMenu map_legend = (FloatingActionsMenu) rootView.findViewById(R.id.map_legend);

        // Enable Scroll Btn and Listener
        final FloatingActionButton action_draggable = (FloatingActionButton) rootView.findViewById(R.id.action_draggable);
        action_draggable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(mMap.getUiSettings().isZoomGesturesEnabled())
                {
                    mMap.getUiSettings().setZoomGesturesEnabled(false);
                    mMap.getUiSettings().setScrollGesturesEnabled(false);
                }
                else
                {
                    mMap.getUiSettings().setZoomGesturesEnabled(true);
                    mMap.getUiSettings().setScrollGesturesEnabled(true);
                    map_legend.collapse();

                }

            }
        });

        // Legend Keys
        final FloatingActionButton legend_low = (FloatingActionButton) rootView.findViewById(R.id.low);
        final FloatingActionButton legend_high = (FloatingActionButton) rootView.findViewById(R.id.high);
        final FloatingActionButton legend_mod = (FloatingActionButton) rootView.findViewById(R.id.moderate);

        final FloatingActionButton action_legend = (FloatingActionButton) rootView.findViewById(R.id.action_legend);
        action_legend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(legend_high.getVisibility() == View.VISIBLE)
                {
                    legend_high.setVisibility(View.GONE);
                    legend_low.setVisibility(View.GONE);
                    legend_mod.setVisibility(View.GONE);
                }
                else
                {
                    legend_high.setVisibility(View.VISIBLE);
                    legend_low.setVisibility(View.VISIBLE);
                    legend_mod.setVisibility(View.VISIBLE);
                }


            }
        });

        // Main Menu Btn for Zoom In, Zoom Out, and Set Up Btn
        final FloatingActionsMenu menuBtn = (FloatingActionsMenu) rootView.findViewById(R.id.proximity_setting);
        menuBtn.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {

                map_legend.collapse();
                beforeRad = radius;
                rootView.findViewById(R.id.mapProximity).setVisibility(View.VISIBLE);
            }

            @Override
            public void onMenuCollapsed() {

                rootView.findViewById(R.id.mapProximity).setVisibility(View.GONE);
            }
        });

        // Main Menu Btn for Map legend and scrollable options
        map_legend.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                menuBtn.collapse();
            }

            @Override
            public void onMenuCollapsed() {

            }
        });

        final FloatingActionButton update_proximity = (FloatingActionButton) rootView.findViewById(R.id.update_location);
        update_proximity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                TextView txt = (TextView) rootView.findViewById(R.id.display);
                String str = txt.getText().toString().replace("km", "");
                int num = Integer.valueOf(str);


                if (num*1000 != beforeRad) {

                    rootView.findViewById(R.id.mapProximity).setVisibility(View.GONE);
                    menuBtn.collapse();

                    proximtyRef.child(user.getUid()).child("profile")
                            .addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                            Intent intent = new Intent(getContext(), LoadDataActivity.class);
                            startActivity(intent);
                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                    proximtyRef.child(user.getUid()).child("profile").child("proximityDist").setValue(num);
                }
            }
        });

        return rootView;
    }

    // Build API request
    public void initialiseMapObject()
    {
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        // Call connection method
        mGoogleApiClient.connect();

        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        // Map Interface Setup
        mMap = googleMap;

        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.setTrafficEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setZoomGesturesEnabled(false);
        mMap.getUiSettings().setScrollGesturesEnabled(false);

        // Location Permissions
        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
            } else {

                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSION_REQUEST_READ_FINE_LOCATION);

            }
        }
        mMap.setMyLocationEnabled(true);


    }


    // Method for adding new Markers to map
    private void handleNewLocation(Location location, String fname, int score) {
        Log.d(TAG, location.toString());

        double currentLatitude = location.getLatitude();
        double currentLongitude = location.getLongitude();

        Drawable marker = getResources().getDrawable(R.drawable.ic_location_on);

        // If statement to change color of icon markers on map.
        if (score >= 70) {
            marker.setColorFilter(Color.parseColor("#F76014"), PorterDuff.Mode.SRC_IN);

        } else if (score >= 40 && score < 70) {
            marker.setColorFilter(Color.parseColor("#14B0BF"), PorterDuff.Mode.SRC_IN);
        } else {
            marker.setColorFilter(Color.parseColor("#66931F"), PorterDuff.Mode.SRC_IN);
        }

        BitmapDescriptor markerIcon = getMarkerIconFromDrawable(marker);

        LatLng latLng = new LatLng(currentLatitude, currentLongitude);

        latLng = getRandomLocation(latLng, radius);
        MarkerOptions options = new MarkerOptions()
                .position(latLng)
                .title(fname)
                .icon(markerIcon);

        mMap.addMarker(options);

    }

    // Get Marker Icon
    private BitmapDescriptor getMarkerIconFromDrawable(Drawable drawable) {
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    @Override
    public void onConnected(Bundle bundle) {

        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
            } else {

                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSION_REQUEST_READ_FINE_LOCATION);

            }
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        } else {
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            tempLatLng = latLng;
            initCamera(latLng);
        }
    }


    // Method to Refocus on users Location and arounf circle geofence
    private void initCamera(LatLng location) {

        mMap.clear();

        Circle circle = mMap.addCircle(new CircleOptions()
                .center(location)
                .radius(radius)
                .fillColor(Color.TRANSPARENT)
                .strokeColor(Color.parseColor("#1ABC9C"))
                .strokeWidth(10));
        circle.setVisible(true);

        circle.setClickable(true);

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                circle.getCenter(), (float) getZoomLevel(circle)));

        if (map.keySet().isEmpty() == false) {
            for (String key : map.keySet()) {
                if (map.get(key).getProximity() <= radius/1000) {

                    Location temp = new Location("");
                    temp.setLatitude(map.get(key).getUser().getProfile().getLocation().getLatitude());
                    temp.setLongitude(map.get(key).getUser().getProfile().getLocation().getLongitude());


                    handleNewLocation(temp, map.get(key).getUser().getProfile().getFname(), map.get(key).getScore());
                }
            }
        }
    }

    // Uses the radius value to change the zoom level applied to map
    public double getZoomLevel(Circle circle) {

        double zoomLevel = 16;

        if (radius == 1000) {
            zoomLevel = 13.8;
        } else if (radius == 5000) {
            zoomLevel = 11.5;
        } else if (radius == 10000) {
            zoomLevel = 10.5;
        } else if (radius == 25000) {
            zoomLevel = 9.2;
        } else if (radius == 50000) {
            zoomLevel = 8.2;
        } else if (radius == 100000) {
            zoomLevel = 7.2;
        }

        return zoomLevel;
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    @Override
    public void onLocationChanged(Location location) {
        LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
        initCamera(loc);
    }

    // Randomly places marker on map with circle

    public LatLng getRandomLocation(LatLng point, int radius) {

        List<LatLng> randomPoints = new ArrayList<>();
        List<Float> randomDistances = new ArrayList<>();
        Location myLocation = new Location("");
        myLocation.setLatitude(point.latitude);
        myLocation.setLongitude(point.longitude);

        //This is to generate 10 random points
        for(int i = 0; i<10; i++) {
            double x0 = point.latitude;
            double y0 = point.longitude;

            Random random = new Random();

            // Convert radius from meters to degrees
            double radiusInDegrees = radius / 111000f;

            double u = random.nextDouble();
            double v = random.nextDouble();
            double w = radiusInDegrees * Math.sqrt(u);
            double t = 2 * Math.PI * v;
            double x = w * Math.cos(t);
            double y = w * Math.sin(t);

            // Adjust the x-coordinate for the shrinking of the east-west distances
            double new_x = x / Math.cos(y0);

            double foundLatitude = new_x + x0;
            double foundLongitude = y + y0;
            LatLng randomLatLng = new LatLng(foundLatitude, foundLongitude);
            randomPoints.add(randomLatLng);
            Location l1 = new Location("");
            l1.setLatitude(randomLatLng.latitude);
            l1.setLongitude(randomLatLng.longitude);
            randomDistances.add(l1.distanceTo(myLocation));
        }
        //Get nearest point to the centre
        int indexOfNearestPointToCentre = randomDistances.indexOf(Collections.min(randomDistances));
        return randomPoints.get(indexOfNearestPointToCentre);
    }
}